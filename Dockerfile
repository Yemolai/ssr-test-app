FROM node:14.16-alpine3.10 AS build
RUN npm i -g @quasar/cli
WORKDIR /source
COPY package.json .
COPY yarn.lock .
RUN yarn
COPY . .
RUN npm run build

FROM node:14.16-alpine3.10 AS dist
WORKDIR /app
COPY --from=build /source/dist/ssr .
RUN npm install
EXPOSE 3000
CMD ["node", "index.js"]
