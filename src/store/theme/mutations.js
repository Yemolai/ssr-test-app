import { LOAD } from 'src/store/theme/actionTypes'
import { themeKeys } from 'src/store/theme/constants'

export default {
  [LOAD] (state, payload) {
    const definitions = Object.fromEntries(
      Object.values(themeKeys)
        .filter(color => payload[color])
        .map(color => [color, payload[color]])
    )
    state.definitions = { ...state.definitions, ...definitions }
  }
}
