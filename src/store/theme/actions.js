import { LOAD } from 'src/store/theme/actionTypes'

export function load ({ commit }, payload) {
  if (typeof payload === 'object') {
    commit(LOAD, payload)
  } else {
    throw new Error('payload must be an object')
  }
}
