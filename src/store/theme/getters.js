import { themeKeys } from 'src/store/theme/constants'

export const definitions = ({ definitions = {} }) => Object.fromEntries(
  Object.values(themeKeys)
    .map(colorName => [colorName, definitions[colorName]])
)
