import { themeKeys } from 'src/store/theme/constants'

export default function () {
  return {
    definitions: {
      [themeKeys.PRIMARY]: '#027be3',
      [themeKeys.SECONDARY]: '#26a69a',
      [themeKeys.ACCENT]: '#9c27b0',
      [themeKeys.DARK]: '#1d1d1d',
      [themeKeys.POSITIVE]: '#21ba45',
      [themeKeys.NEGATIVE]: '#c10015',
      [themeKeys.INFO]: '#31ccec',
      [themeKeys.WARNING]: '#f2c037'
    }
  }
}
