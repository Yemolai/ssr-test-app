export const themeKeys = Object.freeze({
  PRIMARY: 'primary',
  SECONDARY: 'secondary',
  ACCENT: 'accent',
  DARK: 'dark',
  POSITIVE: 'positive',
  NEGATIVE: 'negative',
  INFO: 'info',
  WARNING: 'warning'
})
