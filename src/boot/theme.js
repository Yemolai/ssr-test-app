import { colors } from 'quasar'
import { themeKeys } from 'src/store/theme/constants'

const { setBrand } = colors

export const setBrandFromDefinitions = (definitions) => {
  Object.values(themeKeys).forEach(colorName => {
    if (colorName in definitions) {
      setBrand(colorName, definitions[colorName])
    }
  })
}

// "async" is optional;
// more info on params: https://quasar.dev/quasar-cli/boot-files
export default async ({ store, ssrContext }) => {
  if (ssrContext && ssrContext.req && 'theme' in ssrContext.req) {
    const { theme = {} } = ssrContext.req
    await store.dispatch('theme/load', theme)
  } else {
    setBrandFromDefinitions(store.getters['theme/definitions'])
  }
}
